import os

# get project root path
PROJECT_ROOT = os.path.dirname(
    os.path.dirname(
        os.path.dirname(os.path.abspath(__file__))
        )
    )

RAW_FILENAME = 'terminal_data.xlsx'
PROCESSED_FILEPATH = 'processed_terminal_data.csv'

RAW_DATA_FOLDER = os.path.join(PROJECT_ROOT, 'data', 'raw')
PROCESSED_DATA_FOLDER = os.path.join(PROJECT_ROOT, 'data', 'processed')
INTERIM_DATA_FOLDER = os.path.join(PROJECT_ROOT, 'data', 'interim')
EXTERNAL_DATA_FOLDER = os.path.join(PROJECT_ROOT, 'data', 'external')

RAW_FILEPATH = os.path.join(RAW_DATA_FOLDER, RAW_FILENAME)
PROCESSED_FILEPATH = os.path.join(PROCESSED_DATA_FOLDER, PROCESSED_FILEPATH)

TRAIN_FILEPATH = os.path.join(INTERIM_DATA_FOLDER, 'train.csv')

MODELS_FOLDER = os.path.join(PROJECT_ROOT, 'models')

END_OF_TRAIN_DATE = '2022-10-31'

if __name__ == '__main__':
    print(RAW_FILEPATH, PROCESSED_FILEPATH)
