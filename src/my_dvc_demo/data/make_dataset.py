# -*- coding: utf-8 -*-
import logging
from pathlib import Path

from tqdm import tqdm
import pandas as pd
from dotenv import find_dotenv, load_dotenv

from my_dvc_demo.constants import (
    RAW_FILEPATH,
    PROCESSED_FILEPATH,
    TRAIN_FILEPATH
)


def preprocess_raw(input_filepath: str=RAW_FILEPATH,
                   output_filepath: str=PROCESSED_FILEPATH) -> None:
    """Обрабатывает и сохраняет входные данные

    Args:
        input_filepath (str): _description_
        output_filepath (str): _description_
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')

    df_money = pd.read_excel(input_filepath, sheet_name='Incomes')
    df_money.index = df_money['TID']
    df_money.drop(['TID','остаток на 31.08.2022 (входящий)'], axis=1, inplace=True)
    df_money_in = df_money.unstack(level=0)
    df_money_in = df_money_in.reset_index().rename(columns={'level_0':'date',
                                                            0:'money_in'})
    df_money_in['date'] = pd.to_datetime(df_money_in['date'])
    df_money_in.to_csv(output_filepath, index=False)


def get_preprocessed(processed_filepath: str=PROCESSED_FILEPATH):
    """Читает подготовленный файл входных данных

    Args:
        preprocessed_filepath (str, optional): _description_. Defaults to PROCESSED_FILEPATH.

    Returns:
        _type_: _description_
    """
    return pd.read_csv(processed_filepath)


def extract_features_from_date(date_series: pd.Series) -> pd.DataFrame:
    """Извлекает день и месяц из даты

    Args:
        date_series (pd.Series): _description_

    Returns:
        pd.DataFrame: _description_
    """
    features = pd.DataFrame()
    features["day"] = date_series.dt.day
    features["weekday"] = date_series.dt.weekday
    return features


def create_targets(incomes: pd.DataFrame,
                   max_period: int=30) -> pd.DataFrame:
    """Подготавливает таргеты

    Args:
        in_df (pd.DataFrame): _description_
        max_period (int, optional): _description_. Defaults to 30.

    Returns:
        pd.DataFrame: _description_
    """

    all_tids = incomes['TID'].unique().tolist()
    tgt_df_list = []

    for tid in tqdm(all_tids):
        tgt_df = incomes.loc[incomes['TID'] == tid, ['date', 'TID', 'money_in']].copy()

        for shift in range(1, max_period+1):
            tgt_df[f'target_income_{shift}_day_after'] =  tgt_df['money_in'].shift(-shift)

        tgt_df_list.append(tgt_df)
    all_tgt_df = pd.concat(tgt_df_list, ignore_index=True)
    all_tgt_df.drop(columns=['money_in'], inplace=True)
    return all_tgt_df


def create_features(incomes: pd.DataFrame,
                    max_period: int=30):
    """Подготавливает признаки

    Args:
        in_df (pd.DataFrame): _description_
        max_period (int, optional): _description_. Defaults to 30.
    """
    all_tids = incomes['TID'].unique().tolist()
    diff_df_list = []

    for tid in tqdm(all_tids):
        diff_df = incomes.loc[incomes['TID'] == tid, ['date', 'TID', 'money_in']].copy()
        diff_df.loc[:, 'yesterday_income_growth'] = diff_df['money_in'].diff(1).shift()
        diff_df.loc[:, 'yesterday_growth_of_income_growth'] = \
            diff_df['yesterday_income_growth'].diff(1).shift()

        for shift in range(1, max_period+1):
            diff_df[f'income_{shift}_day_before'] = diff_df['money_in'].shift(shift)

        diff_df['mean_income_all'] = diff_df['money_in'].mean()
        diff_df['mean_income_last_30_days'] = diff_df['money_in'].rolling(window=30).mean().shift(1)
        diff_df['mean_income_last_7_days'] = diff_df['money_in'].rolling(window=7).mean().shift(1)
        diff_df['mean_income_last_3_days'] = diff_df['money_in'].rolling(window=3).mean().shift(1)

        diff_df_list.append(diff_df)

    all_diff_df = pd.concat(diff_df_list, ignore_index=True)
    all_diff_df.drop(columns=['money_in'], inplace=True)

    return all_diff_df


def create_train(processed_filepath: str=PROCESSED_FILEPATH,
                 train_filepath: str=TRAIN_FILEPATH) -> pd.DataFrame:
    """Подготавливает обучающую выборку

    Args:
        processed_filepath (str, optional): _description_. Defaults to PROCESSED_FILEPATH.
        train_filepath (str, optional): _description_. Defaults to TRAIN_FILEPATH.

    Returns:
        pd.DataFrame: _description_
    """
    incomes = get_preprocessed(processed_filepath)
    features_df = create_features(incomes)
    targets_df = create_targets(incomes)

    train_data = incomes.merge(features_df).merge(targets_df)
    train_data.sort_values(by=['date', 'TID'], inplace=True)
    train_data.to_csv(train_filepath, index=False)


def get_train(train_filepath: str=TRAIN_FILEPATH) -> pd.DataFrame:
    """Считывает подготовленную обучающую выборку

    Args:
        train_filepath (str, optional): _description_. Defaults to TRAIN_FILEPATH.

    Returns:
        pd.DataFrame: _description_
    """
    train = pd.read_csv(train_filepath)
    train['date'] = pd.to_datetime(train['date'])
    return train.set_index('date')


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    preprocess_raw(RAW_FILEPATH, PROCESSED_FILEPATH)
    incomes = get_preprocessed(PROCESSED_FILEPATH)
    print(incomes)
    create_train(PROCESSED_FILEPATH, TRAIN_FILEPATH)
    train = get_train(TRAIN_FILEPATH)
    print(train)
