# dvc_s3_demo



## Репозиторий создан для демонстрации работы с s3 хранилищем и dvc

Внутри простой код для обучения ML-модели и работы с данными + пайплайн по созданию и оформлению структуры папок с помощью cookiecutter

## Установка и настройка

Протестировано с python 3.10.2

Предполагается, что вы используете для контроля версий pyenv с плагином pyenv-virtualenv
```
pyenv install 3.10.2
pyenv virtualenv 3.10.2 s3demo
git clone git@gitlab.com:Affernus/dvc_s3_demo.git
cd dvc_s3_demo
local s3demo
pip install pip --upgrade
pip install -r requirements.txt
```