import os
from typing import Optional, Dict

import pandas as pd
import numpy as np
from tqdm import tqdm

import lightgbm as lgb

from my_dvc_demo.data import make_dataset
from my_dvc_demo.constants import (
    TRAIN_FILEPATH,
    MODELS_FOLDER
)

def extract_dayofyear_series(train_data: pd.DataFrame) -> pd.Series:
    """Вытаскивает из трейна день года
    Нужен для разбиения на трейн-валидацию-тест

    Args:
        train_data (pd.DataFrame): _description_

    Returns:
        pd.Series: _description_
    """
    dayofyear = pd.Series(data=train_data.index,
                          index=train_data.index)

    dayofyear = dayofyear.dt.dayofyear
    return dayofyear


def filter_train_by_needed_days(train_data: pd.DataFrame,
                                needed_days: list) -> pd.DataFrame:
    """Фильтрует трейн по нужным дням года
    Нужен для разбиения на трейн-валидацию-тест

    Args:
        train_data (pd.DataFrame): _description_
        needed_days (list): _description_

    Returns:
        pd.DataFrame: _description_
    """
    train_data = train_data.copy()
    dayofyear = extract_dayofyear_series(train_data)
    return train_data[dayofyear.isin(needed_days)]


def train_lgbm_models(train_filepath: str=TRAIN_FILEPATH,
                      max_forecast_horizon: int=30,
                      private_days_count: int=30,
                      n_iterations: int=5,
                      model_parameters: Optional[Dict]=None) -> Dict:
    """Обучает градиентный бустниг

    Args:
        max_forecast_horizon (int, optional): _description_. Defaults to 30.
        private_days_count (int, optional): _description_. Defaults to 30.
        n_iterations (int, optional): _description_. Defaults to 5.
        model_parameters (Optional[Dict], optional): _description_. Defaults to None.

    Raises:
        ValueError: _description_
        ValueError: _description_

    Returns:
        Dict: _description_
    """
    if model_parameters is None:
        model_parameters = dict(
            n_estimators=1000,
            num_leaves=31,
            bagging_fraction=.7,
            feature_fraction=.7,
            random_state=27,
            early_stopping_round=5
    )

    # загружаем трейн
    train_data = make_dataset.get_train(train_filepath)

    # разделяем его по дням года на публичную и приватную часть
    dayofyear = extract_dayofyear_series(train_data)

    unique_dayofyear = dayofyear.unique()
    unique_dayofyear.sort()

    public_days = unique_dayofyear[:-private_days_count]

    # колонки с признаками
    features_cols = [col for col in train_data.columns if 'target' not in col]

    # колонка с true target (её значение берем для валидации за нужный день)
    true_target = 'money_in'

    for forecast_horizon in tqdm(range(1, max_forecast_horizon + 1)):
        train_days = public_days[:-forecast_horizon]
        val_days = [public_days[-1]]

        if val_days[0] - train_days[-1] != forecast_horizon:
            raise ValueError('Ошибка в расчете дней для обучения')

        # на какой таргет учимся
        needed_target = f'target_income_{forecast_horizon}_day_after'

        # обучаемся с валидацией и находим лучшее количество решателей
        best_iterations = []

        for i in range( - n_iterations + 1, 1 ):
            shift = i
            if i == 0:
                shift = None

            x_train = filter_train_by_needed_days(train_data, train_days[:shift])
            x_val = filter_train_by_needed_days(train_data, [val_days[0]-i])

            # разделяем на признаки и таргеты
            x_train, y_train = x_train[features_cols], x_train[needed_target]
            x_val, y_val = x_val[features_cols], x_val[true_target]

            model = lgb.LGBMRegressor(**model_parameters)

            model.fit(
                x_train, y_train,
                eval_set=[(x_val, y_val)],
                eval_metric='mae'
            )
            best_iterations.append(model.best_iteration_)

        new_params = {k: v for k, v in model_parameters.items()
                      if k not in {'early_stopping_round', 'n_estimators'}}

        new_params['early_stopping_round'] = None
        new_params['n_estimators'] = int(np.mean(best_iterations))

        # расширяем трейн и обучаемся на всех данных с подобранным числом
        # решателей
        extended_train_days = list(range(train_days[0],
                                        val_days[-1] + 1,
                                        1))

        if extended_train_days != list(public_days):
            print(extended_train_days, public_days)
            raise ValueError('Ошибка в расчете дней для обучения')

        # собираем полный трейн и обучаем модель
        new_train = filter_train_by_needed_days(train_data, extended_train_days)

        new_x_train, new_y_train = new_train[features_cols], new_train[needed_target]
        model = lgb.LGBMRegressor(**new_params)

        model.fit(new_x_train, new_y_train)

        path = os.path.join(MODELS_FOLDER, f'{forecast_horizon}.txt')
        model.booster_.save_model(path)


if __name__ == '__main__':
    train_lgbm_models(TRAIN_FILEPATH)
