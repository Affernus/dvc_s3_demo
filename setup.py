from setuptools import find_packages, setup

setup(
    name='dvc_demo',
    version='0.1.0',
    author='Affernus',
    author_email='',
    description='toy project for dvc vs s3 storage example',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    package_data={
        },
    include_package_data=True,
    extras_require={
        'test': [
            'pycodestyle',
            'pylint',
            'pylint-quotes',
            'pytest',
            'pytest-cov',
            'pytest-mock',
            'pytest-pythonpath',
            'flake8',
            'bandit'
        ],
    }
)
