import os
from typing import Union
from warnings import warn

import numpy as np
import pandas as pd
import lightgbm as lgb

from my_dvc_demo.data import make_dataset
from my_dvc_demo.constants import (
    MODELS_FOLDER,
    END_OF_TRAIN_DATE
)


class IncomeForecastLGBM():
    """Прогноз income бустингом
    """
    def __init__(self) -> None:
        self.models = self.__load_models()
        if not self.models:
            raise ValueError('Модели не найдены, запустите forecast.train_lgbm_models()')

        self.train = make_dataset.get_train()
        features = [col for col in self.train.columns if 'target' not in col]
        self.train = self.train[features]


    def __load_models(self):
        models = {}
        for file in os.listdir(MODELS_FOLDER):
            if file.endswith('.txt'):
                model_name = int(file.split('.')[0])
                models[model_name] = lgb.Booster(
                    model_file=os.path.join(MODELS_FOLDER, file)
                )
        return models


    def __predict_by_mean(self,
                          today_date: Union[str, np.datetime64],
                          n_periods: int) -> pd.DataFrame:
        """Возвращает среднее по всем историческим данным

        Args:
            today_date (Union[str, np.datetime64]): _description_
            n_periods (int): _description_
        """
        predictions_list = []
        train_cond = self.train.index <= pd.to_datetime(END_OF_TRAIN_DATE)
        mean_forecast = self.train[train_cond].groupby('TID')['money_in'].mean()
        mean_forecast = mean_forecast.reset_index()

        for i in range(1, n_periods + 1):
            result = mean_forecast.copy()
            result['date'] = today_date + pd.Timedelta(days=i)
            predictions_list.append(result)

        return pd.concat(predictions_list)[['date', 'TID', 'money_in']]


    def predict(self,
                today_date: Union[str, np.datetime64],
                n_periods: int) -> pd.DataFrame:
        """_summary_

        Args:
            today_date (str | np.datetime64): "сегодняшняя" дата
            n_periods (int): на сколько дней вперед делать прогноз

        Returns:
            pd.DataFrame: _description_
        """
        today_date = pd.to_datetime(today_date)
        cond = self.train.index == today_date

        slice_df = self.train[cond]

        if slice_df.empty:
            warn(f'Дата {today_date} не найдена в данных, прогнозируем средним по TID')
            return self.__predict_by_mean(today_date, n_periods)

        predictions_list = []

        for i in range(1, n_periods + 1):

            if i not in self.models:
                warn('Горизонт прогноза слишком большой, прогнозируем средним по TID')
                pred_vals = []

                for _, mdl in self.models.items():
                    pred_vals.append(mdl.predict(slice_df))

                pred_vals = np.concatenate(
                    pred_vals
                    ).reshape(len(self.models), slice_df.shape[0])
                pred_vals = pred_vals.mean(axis=0)

            else:
                pred_vals = self.models[i].predict(slice_df)

            predictions = pd.DataFrame(pred_vals, columns=['money_in'])
            predictions['TID'] = slice_df['TID'].values
            predictions['date'] = today_date + pd.Timedelta(days=i)
            predictions_list.append(predictions)

        result = pd.concat(predictions_list)[['date', 'TID', 'money_in']]

        return result

if __name__ == '__main__':
    model = IncomeForecastLGBM()
    predictions = model.predict('2022-11-01', n_periods=10)
    print(predictions)
