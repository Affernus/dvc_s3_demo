"""Запускает весь пайплайн
"""
import logging
from dotenv import find_dotenv, load_dotenv

from my_dvc_demo.data import make_dataset
from my_dvc_demo.models import model, train_model
from my_dvc_demo.constants import (
    RAW_FILEPATH,
    PROCESSED_FILEPATH,
    TRAIN_FILEPATH,

)

if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    make_dataset.preprocess_raw(RAW_FILEPATH, PROCESSED_FILEPATH)
    incomes = make_dataset.get_preprocessed(PROCESSED_FILEPATH)

    print('Preprocessed raw:\n')
    print(incomes)
    make_dataset.create_train(PROCESSED_FILEPATH, TRAIN_FILEPATH)
    train = make_dataset.get_train(TRAIN_FILEPATH)

    print('Train:\n')
    print(train)

    train_model.train_lgbm_models(TRAIN_FILEPATH)

    my_model = model.IncomeForecastLGBM()
    predictions = my_model.predict('2022-11-01', n_periods=10)
    print(predictions)
